<?php

namespace ContextualCode\VarnishBundle\Controller;

use eZ\Bundle\EzPublishLegacyBundle\Controller\LegacyKernelController as eZLegacyKernelController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ContextualCode\VarnishBundle\DependencyInjection\ContextualCodeVarnishExtension;
use ContextualCode\VarnishBundle\Classes\InstallationId;

class LegacyKernelController extends eZLegacyKernelController
{
    public function indexAction()
    {
        $response = parent::indexAction();
        $response->headers->set( ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER, InstallationId::get() );
        return $response;
    }
}