<?php

namespace ContextualCode\VarnishBundle\Controller;

use eZ\Publish\Core\MVC\Symfony\Controller\Content\ViewController as eZViewController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ContextualCode\VarnishBundle\DependencyInjection\ContextualCodeVarnishExtension;
use ContextualCode\VarnishBundle\Classes\InstallationId;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;

class ViewController extends eZViewController
{
    
    public function viewAction(ContentView $view)
    {
        $response = new Response();
        $response->headers->set( ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER, InstallationId::get() );
        $view->setResponse($response);
        return $view;
    }

    protected function buildResponse( $etag = null, \DateTime $lastModified = null )
    {
        $response = parent::buildResponse($etag, $lastModified);
        $response->headers->set( ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER, InstallationId::get() );
        return $response;
    }

}