<?php

namespace ContextualCode\VarnishBundle\Controller;

use eZ\Bundle\EzPublishLegacyBundle\Controller\PreviewController as eZLegacyPreviewController;
use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;

class LegacyPreviewController extends eZLegacyPreviewController
{
    public function previewContentAction( $contentId, $versionNo, $language, $siteAccessName = null )
    {
        try {
            $response = parent::previewContentAction($contentId, $versionNo, $language, $siteAccessName);
        } catch (NotFoundException $e) {
            $response = new Response();
            $response->setStatusCode(301);
            $response->headers->set('Location', '/content/versionview/');
        }
        // no caching of previews
        $response->setPublic();
        $response->setSharedMaxAge(0);
        $response->setMaxAge(0);
        return $response;
    }
}
