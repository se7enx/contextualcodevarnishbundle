<?php

namespace ContextualCode\VarnishBundle\Services;

use FOS\HttpCacheBundle\CacheManager as FOSCacheManager;
use FOS\HttpCache\ProxyClient\ProxyClientInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CacheManager extends FOSCacheManager
{

    private $cache;

    public function __construct(ProxyClientInterface $cache, UrlGeneratorInterface $urlGenerator)
    {
        parent::__construct($cache, $urlGenerator);
        $this->cache = $cache;
    }

    public function setCacheServers(array $servers)
    {
        return $this->cache->setServers($servers);
    }

}