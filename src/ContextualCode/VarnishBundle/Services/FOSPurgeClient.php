<?php


namespace ContextualCode\VarnishBundle\Services;

use eZ\Publish\Core\MVC\Symfony\Cache\PurgeClientInterface;
use FOS\HttpCacheBundle\CacheManager;
use ContextualCode\VarnishBundle\Classes\InstallationId;
use ContextualCode\VarnishBundle\DependencyInjection\ContextualCodeVarnishExtension;
use ContextualCode\VarnishBundle\Services\PurgeServerList;

class FOSPurgeClient implements PurgeClientInterface
{

    private $cacheManager;
    private $purgeServerListService;
    private $installId;

    public function __construct( CacheManager $cacheManager, PurgeServerList $purgeServerListService )
    {
        $this->cacheManager = $cacheManager;
        $this->purgeServerListService = $purgeServerListService;
        $this->installId = InstallationId::get();
    }

    private function setPurgeServers()
    {
        $purgeServers = $this->purgeServerListService->fetchPurgeList();
        if (!$purgeServers) {
            return;
        }
        $this->cacheManager->setCacheServers($purgeServers);
    }

    public function purge( $locationIds )
    {
        if ( empty( $locationIds ) )
        {
            return;
        }

        if ( !is_array( $locationIds ) )
        {
            $locationIds = array( $locationIds );
        }

        $this->setPurgeServers();
        $this->cacheManager->invalidate( array( 
            ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER => $this->installId,
            'X-Location-Id' => '(' . implode( '|', $locationIds ) . ')' 
        ) );
    }

    public function purgeAll()
    {
        $this->setPurgeServers();
        $this->cacheManager->invalidate( array(
            ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER => $this->installId,
            'X-Location-Id' => '.*' 
        ) );
    }
}
