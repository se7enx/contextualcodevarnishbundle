<?php

namespace ContextualCode\VarnishBundle\Services;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use eZ\Publish\Core\MVC\Symfony\Cache\PurgeClientInterface;
use ContextualCode\VarnishBundle\Classes\InstallationId;
use ContextualCode\VarnishBundle\DependencyInjection\ContextualCodeVarnishExtension;

class PurgeServerList
{

    /**
     * Name of parameter containing port number for all purge servers
     * @var string
     */
    const PURGE_SERVER_PORT_PARAM = "contextual_code_varnish.purge_server_port";

    /**
     * Name of parameter containing purge server list file path
     * @var string
     */
    const PURGE_SERVER_LIST_FILE_PARAM = "contextual_code_varnish.purge_server_list_file";

    /**
     * Name of parameter containing purge server list
     * @var string
     */
    const PURGE_SERVER_LIST_PARAM = "contextual_code_varnish.purge_server_list";

    /**
     * Purge server protocol
     * @var string
     */
    const PURGE_SERVER_PROTOCOL = "http";

    /**
     * List of eZ Publish site groups that should contain purge server configuration
     * @var array
     * @todo dynamic configuration of this?
     */ 
    static public $ezPurgeServerSiteGroups = array("default", "frontend_group", "site_group");

    /**
     * Service container
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Set purge server port
     * @param integer $port
     */
    public function setPurgePort($port)
    {
        if (!is_integer($port)) {
            return;
        }
        $this->container->setParameter(
            self::PURGE_SERVER_PORT_PARAM,
            $port
        );
    }

    /**
     * Set purge list file
     * @param string $filename
     */
    public function setPurgeFile($filename)
    {
        if (!$filename || !is_file($filename)) {
            $this->container->setParameter(self::PURGE_SERVER_LIST_FILE_PARAM, array());
            return;
        }
        $this->container->setParameter(
            self::PURGE_SERVER_LIST_FILE_PARAM,
            array(
                $filename,
                0
            )
        );
    }

    /**
     * Return true if purge server list file exists
     * @return boolean
     */
    public function purgeFileExists()
    {
        $purgeServerListFile = $this->container->getParameter(self::PURGE_SERVER_LIST_FILE_PARAM);
        if (!$purgeServerListFile || !is_array($purgeServerListFile) || count($purgeServerListFile) != 2) {
            return false;
        }
        return is_file($purgeServerListFile[0]);
    }

    /**
     * Return true if purge file has been updated since last fetch
     * @return boolean
     */
    public function purgeFileUpdated()
    {
        if (!$this->purgeFileExists()) {
            return false;
        }
        $purgeServerListFile = $this->container->getParameter(self::PURGE_SERVER_LIST_FILE_PARAM);
        return (filemtime($purgeServerListFile[0]) > $purgeServerListFile[1]);
    }

    /**
     * Fetch purge server list
     * @return array
     */
    public function fetchPurgeList()
    {
        if (!$this->purgeFileExists()) {
            return array();
        }
        if (!$this->purgeFileUpdated()) {
            return $this->container->getParameter(self::PURGE_SERVER_LIST_PARAM);
        }
        $purgeServerListFile = $this->container->getParameter(self::PURGE_SERVER_LIST_FILE_PARAM);
        $purgeServers = file(
            $purgeServerListFile[0],
            FILE_SKIP_EMPTY_LINES
        );
        if (!is_array($purgeServers) || count($purgeServers) == 0) {
            return array();
        }
        foreach ($purgeServers as &$value) {
            if (!trim($value)) {
                unset($value);
                continue;
            }
            $value = sprintf(
                "%s://%s:%d",
                self::PURGE_SERVER_PROTOCOL,
                trim($value),
                $this->container->getParameter(self::PURGE_SERVER_PORT_PARAM)
            );
        }
        $purgeServers = array_filter($purgeServers);
        $purgeServerListFile[1] = filemtime($purgeServerListFile[0]);
        if ($this->container instanceof ContainerBuilder) {
            $this->container->setParameter(self::PURGE_SERVER_LIST_FILE_PARAM, $purgeServerListFile);
            $this->container->setParameter(self::PURGE_SERVER_LIST_PARAM, $purgeServers);
        }
        return $purgeServers;
    }

    /**
     * Fetch and apply purge server list to eZ Publish
     */
    public function setEZPurgeServers()
    {
        if (!($this->container instanceof ContainerBuilder)) {
            return;
        }
        $purgeServerList = $this->fetchPurgeList();
        if (!$purgeServerList) {
            return;
        }
        foreach (self::$ezPurgeServerSiteGroups as $siteGroup) {
            $allPurgeServers = array();
            if ($this->container->hasParameter("ezsettings.{$siteGroup}.http_cache.purge_servers")) {
                $allPurgeServers = $this->container->getParameter("ezsettings.{$siteGroup}.http_cache.purge_servers");
            }
            $allPurgeServers = array_values( array_unique( array_merge($allPurgeServers, $purgeServerList) ) );
            $this->container->setParameter("ezsettings.{$siteGroup}.http_cache.purge_servers", $allPurgeServers);
        }
    }

}