<?php

namespace ContextualCode\VarnishBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeVarnishBundle extends Bundle
{

	protected $name = "ContextualCodeVarnishBundle";

}
