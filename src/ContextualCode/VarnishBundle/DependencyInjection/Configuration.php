<?php

namespace ContextualCode\VarnishBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {

        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('contextual_code_varnish');

        $rootNode
            ->children()
                ->integerNode("purge_server_port")
                    ->defaultValue(6081)
                ->end()
                ->scalarNode("purge_server_list_file")
                    ->defaultValue("/var/www/aws_instances_ips")
                ->end()
            ->end()
        ;

        return $treeBuilder;

    }
}