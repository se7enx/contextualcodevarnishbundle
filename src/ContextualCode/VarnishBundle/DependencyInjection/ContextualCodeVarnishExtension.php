<?php

namespace ContextualCode\VarnishBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;
use eZSolr;
use ContextualCode\VarnishBundle\Services\PurgeServerList;

class ContextualCodeVarnishExtension extends Extension
{

    /**
     * Header name to storage installation id
     * with.
     * @var string
     */
    const PURGE_INSTALL_ID_HEADER = "X-Installation-Id";

    public function load(array $configs, ContainerBuilder $container) {
        $YamlFileLoader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('services.yml');

        // process configuration
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        // retrieve path to purge server list file and save file details as parameter
        $purgeServerListService = new PurgeServerList($container);
        $purgeServerListService->setPurgePort($config["purge_server_port"]);
        $purgeServerListService->setPurgeFile($config["purge_server_list_file"]);

        // update purge servers
        $purgeServerListService->setEZPurgeServers();
        
    }

    public function getAlias() {
        return 'contextual_code_varnish';
    }

}