<?php

namespace ContextualCode\VarnishBundle\Classes;

class InstallationId
{
    static protected $installationId;

    /**
     * Create an installation id by making a
     * hash of a file unique to this site. In this
     * case we'll simply use the file path to this script.
     * @return string
     */
    static public function get()
    {
        if (self::$installationId) {
            return self::$installationId;
        }
        self::$installationId = md5( __FILE__ );
        return self::$installationId;
    }
}