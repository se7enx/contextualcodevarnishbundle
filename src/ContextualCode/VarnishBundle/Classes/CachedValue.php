<?php

namespace ContextualCode\VarnishBundle\Classes;

use eZ\Publish\Core\REST\Server\Output\ValueObjectVisitor\CachedValue as eZCachedValue;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use eZ\Publish\Core\REST\Common\Output\ValueObjectVisitor;
use eZ\Publish\Core\REST\Common\Output\Generator;
use eZ\Publish\Core\REST\Common\Output\Visitor;
use ContextualCode\VarnishBundle\Classes\InstallationId;
use ContextualCode\VarnishBundle\DependencyInjection\ContextualCodeVarnishExtension;


/**
 * CachedValue value object visitor.
 */
class CachedValue extends eZCachedValue
{
    public function visit(Visitor $visitor, Generator $generator, $data)
    {
        parent::visit($visitor, $generator, $data);
        $visitor->getResponse()->headers->set(ContextualCodeVarnishExtension::PURGE_INSTALL_ID_HEADER, InstallationId::get());
    }
}